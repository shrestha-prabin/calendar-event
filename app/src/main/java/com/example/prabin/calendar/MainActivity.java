package com.example.prabin.calendar;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

import com.applandeo.materialcalendarview.CalendarView;
import com.applandeo.materialcalendarview.EventDay;
import com.applandeo.materialcalendarview.exceptions.OutOfDateRangeException;
import com.applandeo.materialcalendarview.listeners.OnDayClickListener;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    CalendarView calendarView;
    TextView tvEvents;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        calendarView = findViewById(R.id.calendarView);
        tvEvents = findViewById(R.id.events);

        try {
            calendarView.setDate(Calendar.getInstance());
        } catch (OutOfDateRangeException e) {
            e.printStackTrace();
        }

        //addTestDataToFirebase();
        markEventToCalendar();

        calendarView.setOnDayClickListener(new OnDayClickListener() {
            @Override
            public void onDayClick(EventDay eventDay) {
                Calendar clickedDay = eventDay.getCalendar();
                int year = clickedDay.get(Calendar.YEAR);
                int month = clickedDay.get(Calendar.MONTH);
                int day = clickedDay.get(Calendar.DAY_OF_MONTH);
                showEventsForDay(year, month, day);
            }
        });
    }

    //added by admin
    private void addTestDataToFirebase() {

        Event e1 = new Event("DSAP Exam", 2018, 7, 1);
        Event e2 = new Event("Project Final Presentation", 2018, 7, 6);
        Event e3 = new Event("Project Demo", 2018, 7, 6);
        Event e4 = new Event("Exhibition", 2018, 7, 11);

        DatabaseReference eventRef = FirebaseDatabase.getInstance()
                .getReference().child("events");

        eventRef.push().setValue(e1);
        eventRef.push().setValue(e2);
        eventRef.push().setValue(e3);
        eventRef.push().setValue(e4);
    }

    private void markEventToCalendar() {

        final List<EventDay> events = new ArrayList<>();

        DatabaseReference eventRef = FirebaseDatabase.getInstance()
                .getReference().child("events");
        eventRef.keepSynced(true);

        eventRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                try {
                    for (DataSnapshot dsp : dataSnapshot.getChildren()) {
                        Event e = dsp.getValue(Event.class);
                        Calendar eventDay = Calendar.getInstance();
                        eventDay.set(e.getYear(), e.getMonth(), e.getDay());
                        events.add(new EventDay(eventDay, android.R.drawable.ic_delete));
                    }
                    calendarView.setEvents(events);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    private void showEventsForDay(final int year, final int month, final int day) {

        DatabaseReference eventRef = FirebaseDatabase.getInstance()
                .getReference().child("events");
        eventRef.keepSynced(true);

        eventRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                try {
                    StringBuilder events = new StringBuilder();
                    for (DataSnapshot dsp : dataSnapshot.getChildren()) {
                        Event e = dsp.getValue(Event.class);
                        if(e.getYear() == year && e.getMonth() == month && e.getDay() == day) {
                            events.append(e.getTitle() + "\n");
                        }
                    }
                    tvEvents.setText(events);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

}
