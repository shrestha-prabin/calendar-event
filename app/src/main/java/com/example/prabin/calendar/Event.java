package com.example.prabin.calendar;

import java.util.Calendar;

/**
 * Created by Prabin on 7/27/2018.
 */

public class Event {

    int day;
    int month;
    String title;
    int year;

    public Event() {
    }

    public Event(String title, int year, int month, int day) {
        this.title = title;
        this.year = year;
        this.month = month;
        this.day = day;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public int getMonth() {
        return month;
    }

    public void setMonth(int month) {
        this.month = month;
    }

    public int getDay() {
        return day;
    }

    public void setDay(int day) {
        this.day = day;
    }
}
